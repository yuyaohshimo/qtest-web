const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const staticPath = path.resolve(process.cwd(), 'build');
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(staticPath));

// mocking API
app.get('/api/users', (req, res) => {
  res.json([
    {
      id: 1,
      name: 'Steve Jobs',
      score: 10,
      created_at: '2016-01-01 00:00:00',
      updated_at: '2016-01-01 00:00:00'
    },
    {
      id: 2,
      name: 'Donald Arthur Norman',
      score: 10,
      created_at: '2016-01-01 00:00:00',
      updated_at: '2016-01-01 00:00:00'
    },
    {
      id: 3,
      name: 'Stephen Gary Wozniak',
      score: 8,
      created_at: '2016-01-01 00:00:00',
      updated_at: '2016-01-01 00:00:00'
    },
    {
      id: 4,
      name: 'Isaac Newton',
      score: 7,
      created_at: '2016-01-01 00:00:00',
      updated_at: '2016-01-01 00:00:00'
    },
    {
      id: 5,
      name: 'Galileo Galilei',
      score: 6,
      created_at: '2016-01-01 00:00:00',
      updated_at: '2016-01-01 00:00:00'
    },
    {
      id: 6,
      name: 'Charles Darwin',
      score: 5,
      created_at: '2016-01-01 00:00:00',
      updated_at: '2016-01-01 00:00:00'
    },
    {
      id: 7,
      name: 'Thomas Edison',
      score: 4,
      created_at: '2016-01-01 00:00:00',
      updated_at: '2016-01-01 00:00:00'
    },
    {
      id: 8,
      name: 'Albert Einstein',
      score: 3,
      created_at: '2016-01-01 00:00:00',
      updated_at: '2016-01-01 00:00:00'
    },
    {
      id: 9,
      name: 'Joseph Louis Gay-Lussac',
      score: 2,
      created_at: '2016-01-01 00:00:00',
      updated_at: '2016-01-01 00:00:00'
    },
    {
      id: 10,
      name: 'Maria Skłodowska-Curie',
      score: 2,
      created_at: '2016-01-01 00:00:00',
      updated_at: '2016-01-01 00:00:00'
    }
  ])
});

app.get('/api/questions', (req, res) => {
  res.json([
    {
      id: 1,
      content: 'How many vowels are there in the English alphabet?',
      answer: '5',
      created_at: '2016-01-01 00:00:00',
      updated_at: '2016-01-01 00:00:00'
    },
    {
      id: 2,
      content: 'How many vowels are there in the English alphabet?',
      answer: '5',
      created_at: '2016-01-01 00:00:00',
      updated_at: '2016-01-01 00:00:00'
    }
  ])
});

app.post('/api/users', (req, res) => {
  const name = req.body.name;
  const score = req.body.score;
  res.status(201).json({
    id: 1,
    name: name,
    score: score,
    created_at: '2016-01-01 00:00:00',
    updated_at: '2016-01-01 00:00:00'
  });
});

app.get('*', (req, res) => res.sendFile(path.resolve(staticPath, 'index.html')));
app.listen(8080);
