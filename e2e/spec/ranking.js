const url = 'http://localhost:8080/ranking';

module.exports = {
  'Show ranking' : function (browser) {
    browser
    .url(url)
    .waitForElementPresent('#ranking', 5000)
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(1) > td:nth-child(1)', '1')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(1) > td:nth-child(2)', 'Steve Jobs')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(1) > td:nth-child(3)', '10')

    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(2) > td:nth-child(1)', '1')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(2) > td:nth-child(2)', 'Donald Arthur Norman')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(2) > td:nth-child(3)', '10')

    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(3) > td:nth-child(1)', '3')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(3) > td:nth-child(2)', 'Stephen Gary Wozniak')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(3) > td:nth-child(3)', '8')

    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(4) > td:nth-child(1)', '4')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(4) > td:nth-child(2)', 'Isaac Newton')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(4) > td:nth-child(3)', '7')

    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(5) > td:nth-child(1)', '5')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(5) > td:nth-child(2)', 'Galileo Galilei')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(5) > td:nth-child(3)', '6')

    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(6) > td:nth-child(1)', '6')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(6) > td:nth-child(2)', 'Charles Darwin')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(6) > td:nth-child(3)', '5')

    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(7) > td:nth-child(1)', '7')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(7) > td:nth-child(2)', 'Thomas Edison')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(7) > td:nth-child(3)', '4')

    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(8) > td:nth-child(1)', '8')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(8) > td:nth-child(2)', 'Albert Einstein')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(8) > td:nth-child(3)', '3')

    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(9) > td:nth-child(1)', '9')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(9) > td:nth-child(2)', 'Joseph Louis Gay-Lussac')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(9) > td:nth-child(3)', '2')

    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(10) > td:nth-child(1)', '9')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(10) > td:nth-child(2)', 'Maria Skłodowska-Curie')
    .assert.containsText('#ranking > section > table > tbody > tr:nth-child(10) > td:nth-child(3)', '2');
  },
  after: browser => browser.end()
};
