const url = 'http://localhost:8080';

module.exports = {

  'Send score': function (browser) {
    browser
    .url(url)
    .waitForElementPresent('#top', 5000)

    // name
    .setValue('#top input', 'Steve Jobs')

    // submit
    .click('#top button')

    // question page
    .waitForElementPresent('#question', 5000)
    .assert.urlEquals(url + '/question')

    // answer question
    .setValue('#question input', 'six')
    .click('#question button')

    // result modal
    .waitForElementPresent('.modal', 5000)
    .assert.containsText('.modal .box .title', 'That\'s incorrect.')

    // send score
    .click('.modal button:nth-child(2)')
    .waitForElementNotPresent('#loading', 5000)
    .assert.attributeEquals('.modal button:nth-child(2)', 'disabled', "true")
  },
  after: browser => browser.end()
};
