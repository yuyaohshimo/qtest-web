const url = 'http://localhost:8080';

module.exports = {
  beforeEach: function(browser) {
    browser
    .url(url)
    .waitForElementPresent('#top', 5000)

    // name
    .setValue('#top input', 'Steve Jobs')

    // submit
    .click('#top button');
  },
  'Get and show question': function (browser) {
    browser
    .waitForElementPresent('#question', 5000)
    .assert.urlEquals(url + '/question')
    .assert.containsText('#question h2', 'Question 1/2')
    .assert.containsText('#question .panel p', 'How many vowels are there in the English alphabet?')
  },
  'Answer question with incorrect answer': function(browser) {
    browser
    .waitForElementPresent('#question', 5000)
    .assert.urlEquals(url + '/question')

    // answer question
    .setValue('#question input', 'six')
    .click('#question button')

    // result modal
    .waitForElementPresent('.modal', 5000)
    .assert.containsText('.modal .box .title', 'That\'s incorrect.')

    // got to Top
    .click('.modal button:nth-child(1)')
    .assert.urlEquals(url + '/')
  },
  'Answer all questions with correct answer': function(browser) {
    browser
    .waitForElementPresent('#question', 5000)
    .assert.urlEquals(url + '/question')

    // answer question
    .setValue('#question input', 'five')
    .click('#question button')

    .waitForElementPresent('.modal', 5000)
    .assert.containsText('.modal .box .title', 'That\'s correct.')
    .waitForElementNotPresent('.modal', 5000)

    .assert.containsText('#question h2', 'Question 2/2')

    // answer last question
    .setValue('#question input', 'five')
    .click('#question button')

    // result modal
    .waitForElementPresent('.modal', 5000)
    .assert.containsText('.modal .box .title', 'That\'s correct.')
    .waitForElementNotPresent('.modal', 5000)

    // perfect score
    .assert.containsText('#question .title', 'Congratulation! You\'re a perfect scorer!')

    // go to Top
    .click('#question button:nth-child(1)')
    .assert.urlEquals(url + '/')
  },
  after: browser => browser.end()
};
