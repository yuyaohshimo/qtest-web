import {
  INIT
} from '../constants/app';

import {
  LOAD_QUESTIONS_SUCCESS
} from '../constants/question';

const initialState = [];

export default function question(state = initialState, action) {
  switch (action.type) {
  case INIT:
    return [];
  case LOAD_QUESTIONS_SUCCESS:
    return action.questions;
  default:
    return state;
  }
}
