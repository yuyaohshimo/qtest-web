import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux'
import global from './global';
import user from './user';
import question from './question';
import quiz from './quiz';
import ranking from './ranking';

const rootReducer = combineReducers({
  routing: routerReducer,
  global,
  user,
  question,
  quiz,
  ranking
});

export default rootReducer;
