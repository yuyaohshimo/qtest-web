import {
  LOADING_START,
  LOADING_END,
  ERROR,
  INIT,
  START,
  SEND_SCORE_SUCCESS
} from '../constants/app.js';

const initialState = {
  error: false,
  loading: false,
  scoreSent: false
};

export default function global(state = initialState, action) {
  switch (action.type) {
  case INIT:
    return {
      error: false,
      loading: false,
      scoreSent: false
    }
  case ERROR:
    return Object.assign({}, state, {
      error: action.error
    })
  case LOADING_START:
    return Object.assign({}, state, {
      loading: true
    })
  case LOADING_END:
    return Object.assign({}, state, {
      loading: false
    })
  case START:
    return state;
  case SEND_SCORE_SUCCESS:
    return Object.assign({}, state, {
      scoreSent: true
    })
  default:
    return state;
  }
}
