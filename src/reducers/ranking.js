import {
  INIT
} from '../constants/app';

import {
  LOAD_RANKING,
  LOAD_RANKING_SUCCESS
} from '../constants/ranking';

const initialState = false;

export default function ranking(state = initialState, action) {
  switch (action.type) {
  case INIT:
  case LOAD_RANKING:
    return false;
  case LOAD_RANKING_SUCCESS:
    return action.ranking;
  default:
    return state;
  }
}
