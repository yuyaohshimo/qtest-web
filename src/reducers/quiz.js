import {
  INIT,
  NEXT
} from '../constants/app';

const initialState = {
  correctNum: 0
};

export default function quiz(state = initialState, action) {
  switch (action.type) {
  case INIT:
    return { correctNum: 0 };
  case NEXT:
    return { correctNum: ++state.correctNum }
  default:
    return state;
  }
}
