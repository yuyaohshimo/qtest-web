import {
  INIT
} from '../constants/app';

import {
  SAVE
} from '../constants/user';

const initialState = {
  name: ''
};

export default function user(state = initialState, action) {
  switch (action.type) {
  case INIT:
    return { name: '' };
  case SAVE:
    return action.user;
  default:
    return state;
  }
}
