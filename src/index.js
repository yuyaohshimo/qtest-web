import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import configureStore from './store/configureStore';
import rootSaga from './sagas';

const store = configureStore();
store.runSaga(rootSaga)

const history = syncHistoryWithStore(browserHistory, store);

// component
import App from './containers/App';
import Top from './containers/Top';
import Question from './containers/Question';
import Ranking from './containers/Ranking';
import NotFound from './containers/NotFound';

// css
import 'bulma/css/bulma.css';
import './index.css';

ReactDOM.render(
  <Provider store={store}>
    <div>
      <Router history={history}>
        <Route path="/" component={App}>
          <IndexRoute component={Top}/>
          <Route path="/question" component={Question}/>
          <Route path="/ranking" component={Ranking}/>
          <Route path="*" component={NotFound}/>
        </Route>
      </Router>
    </div>
  </Provider>,
  document.getElementById('root')
);
