import { fork, take, call, put, select } from 'redux-saga/effects';
import { START, SEND_SCORE } from '../constants/app';
import { LOAD_RANKING } from '../constants/ranking';

import {
  startLoading,
  endLoading,
  error
} from '../actions/app';

import { shuffle } from '../utils/array';

import request from '../utils/request';

// questions
import {
  questionsLoaded
} from '../actions/question';

export function* getQuestions() {
  yield put(startLoading());
  const questions = yield call(request, '/api/questions', { method: 'GET' });
  yield put(endLoading());
  if (questions.err) {
    yield put(error(questions.err.message));
  } else {
    yield put(questionsLoaded(shuffle(questions.data)));
  }
}

export function* createStartWatcher() {
  while (yield take(START)) {
    yield call(getQuestions);
  }
}

// ranking
import {
  rankingLoaded
} from '../actions/ranking';

export function* getRanking() {
  yield put(startLoading());
  const ranking = yield call(request, '/api/users?sort_by=score&sort_order=desc', { method: 'GET' });
  yield put(endLoading());
  if (ranking.err) {
    yield put(error(ranking.err.message));
  } else {
    yield put(rankingLoaded(ranking.data));
  }
}
export function* createLoadRankingWatcher() {
  while (yield take(LOAD_RANKING)) {
    yield call(getRanking);
  }
}

// send score
import {
  scoreSent
} from '../actions/app';
export function* sendScore() {
  const options = {
    method: 'POST',
  };
  const name = yield select(state => state.user.name);
  const correctNum = yield select(state => state.quiz.correctNum);
  const body = {
    name: name,
    score: correctNum,
  };
  options.body = JSON.stringify(body);
  yield put(startLoading());
  const result = yield call(request, '/api/users', options);
  yield put(endLoading());
  if (result.err) {
    yield put(error(result.err.message));
  } else {
    yield put(scoreSent());
  }
}

export function* createSendScoreWatcher() {
  while (yield take(SEND_SCORE)) {
    yield call(sendScore);
  }
}

export default function* start() {
  yield fork(createStartWatcher);
  yield fork(createLoadRankingWatcher);
  yield fork(createSendScoreWatcher);
}
