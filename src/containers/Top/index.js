import React, { Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { bindActionCreators } from 'redux'

import { save } from '../../actions/user';
import { start, init } from '../../actions/app';

import Nav from '../../components/Nav';

class Top extends Component {

  constructor(props) {
    super(props);

    this.onChangeName = this.onChangeName.bind(this);
    this.onStart = this.onStart.bind(this);
    this.state = { userName: '' };
  }

  componentWillMount() {
    this.props.init();
  }

  componentDidUpdate() {
    if (this.props.question.length) {
      browserHistory.push('/question');
    }
  }

  onChangeName(event) {
    this.setState({
      userName: event.target.value
    });
  }

  onStart(event) {
    event.preventDefault();
    this.props.save(Object.assign(this.props.user, { name: this.state.userName }));
    this.props.start();
  }

  render() {
    return (
      <div id="top" className="container">
        <Nav />
        <section className="section">
          <form onSubmit={this.onStart}>
            <p className="title">Please enter your name.</p>
            <label className="label">Name</label>
            <p className="control">
              <input className="input" type="text" placeholder="" onChange={this.onChangeName}/>
            </p>
            <p className="control">
              <button type="submit" className="button is-primary" disabled={!this.state.userName}>Start</button>
            </p>
          </form>
        </section>
      </div>
    )
  }
}

Top.propTypes = {
  question: React.PropTypes.array,
  user: React.PropTypes.object,
  save: React.PropTypes.func,
  start: React.PropTypes.func,
  init: React.PropTypes.func
};

const mapStateToProps = (state, ownProps) => ({
  question: state.question,
  user: state.user
});

function mapDispatchToProps(dispatch) {
  return {
    save: bindActionCreators(save, dispatch),
    start: bindActionCreators(start, dispatch),
    init: bindActionCreators(init, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Top);
