import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'

import { loadRanking } from '../../actions/ranking';

import Nav from '../../components/Nav';

import './styles.css';

class Ranking extends Component {

  componentWillMount() {
    this.props.loadRanking();
  }

  render() {
    const {
      ranking
    } = this.props;
    if (!ranking) {
      return null;
    }
    let trs;
    if (ranking.length) {
      let currentRank = 1;
      let lastScore = ranking[0].score;
      trs = [];
      ranking.forEach((item, idx) => {
        if (lastScore !== item.score) {
          currentRank = idx + 1;
        }
        let rank = currentRank;
        trs.push(
          <tr key={`ranking_${idx}`}>
            <td className={`has-text-centered ranking-${currentRank}`}>{rank}</td>
            <td>{item.name}</td>
            <td>{item.score}</td>
          </tr>
        );
        lastScore = item.score;
      });
    }
    return (
      <div id="ranking" className="container">
        <Nav />
        <section className="section">
          <p className="title">User Ranking</p>
          {trs ?
            <table className="table is-striped">
              <thead>
                <tr>
                  <th></th>
                  <th>Name</th>
                  <th>Score</th>
                </tr>
              </thead>
              <tbody>
                {trs}
              </tbody>
            </table>
          : <p>No user...</p>}
        </section>
      </div>
    )
  }
}

Ranking.propTypes = {
  ranking: React.PropTypes.oneOfType([
    React.PropTypes.array,
    React.PropTypes.bool,
  ]),
}

const mapStateToProps = (state, ownProps) => ({
  ranking: state.ranking,
});

function mapDispatchToProps(dispatch) {
  return {
    loadRanking: bindActionCreators(loadRanking, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Ranking);
