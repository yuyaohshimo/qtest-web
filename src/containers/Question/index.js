import React, { Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { bindActionCreators } from 'redux';
import checkAnswer from '../../utils/answer';

import { next, sendScore } from '../../actions/app';

import ResultModal from '../../components/ResultModal';

import './styles.css';

class Question extends Component {

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeAnswer = this.onChangeAnswer.bind(this);
    this.onNext = this.onNext.bind(this);
    this.onSendScore = this.onSendScore.bind(this);
    this.state = { answer: '', messageType: '', isActiveModal: false };
  }

  componentWillMount() {
    if (!this.props.question.length) {
      browserHistory.push('/');
    }
  }

  gotoTop() {
    browserHistory.push('/');
  }

  onChangeAnswer(event) {
    this.setState({
      answer: event.target.value
    });
  }

  onNext() {
    this.props.next();
    if (this.refs.answer) { this.refs.answer.value = ''; }
    this.setState({
      isActiveModal: false,
      answer: ''
    });
  }

  onSendScore() {
    this.props.sendScore();
  }

  onSubmit(event) {
    event.preventDefault();
    const currentQuestion = this.props.question[this.props.quiz.correctNum];
    let messageType = '';
    if (checkAnswer(currentQuestion, this.state.answer)) {
      messageType = 'CORRECT';
      setTimeout(() => {
        this.onNext();
      }, 500);
    } else {
      messageType = 'INCORRECT';
    }
    this.setState({
      isActiveModal: true,
      messageType
    });
  }

  render() {
    if (!this.props.question.length) {
      return null;
    }
    if (this.props.quiz.correctNum >= this.props.question.length) {
      return (
        <div id="question" className="container">
          <section className="section">
            <h2 className="title">Congratulation! You're a perfect scorer!</h2>
            <div className="block">
              <button className="button is-primary" onClick={this.gotoTop}>Top</button>
              <button className="button is-primary is-outlined" onClick={this.props.sendScore} disabled={this.props.scoreSent}>Send Score</button>
            </div>
          </section>
        </div>
      );
    }
    const currentQuestion = this.props.question[this.props.quiz.correctNum];
    return (
      <div>
        <div id="question" className="container">
          <section className="section">
            <h2 className="title">Question {this.props.quiz.correctNum + 1}/{this.props.question.length}</h2>
            <div className="panel">
              <section className="section">
                <p>{currentQuestion.content}</p>
              </section>
            </div>
            <form onSubmit={this.onSubmit}>
              <label className="label">Answer</label>
              <p className="control">
                <input className="input" ref="answer" type="text" placeholder="" onChange={this.onChangeAnswer}/>
              </p>
              <p className="control">
                <button type="submit" className="button is-primary" disabled={!this.state.answer}>Submit My Answer</button>
              </p>
            </form>
          </section>
        </div>
        { this.state.isActiveModal ?
          <ResultModal
            sendScore={this.onSendScore}
            messageType={this.state.messageType}
            scoreSent={this.props.scoreSent}
            answer={currentQuestion.answer}
          /> : null}
      </div>
    )
  }
}

Question.propTypes = {
  question: React.PropTypes.array,
  quiz: React.PropTypes.object,
  scoreSent: React.PropTypes.bool,
  next: React.PropTypes.func,
  sendScore: React.PropTypes.func
};

const mapStateToProps = (state, ownProps) => ({
  question: state.question,
  quiz: state.quiz,
  scoreSent: state.global.scoreSent
});

function mapDispatchToProps(dispatch) {
  return {
    next: bindActionCreators(next, dispatch),
    sendScore: bindActionCreators(sendScore, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Question);
