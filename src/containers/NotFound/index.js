import React, { Component } from 'react';

import Nav from '../../components/Nav';

class NotFound extends Component {
  render() {
    return (
      <div id="not-found" className="container">
        <Nav />
        <section className="section">
          <p className="title">Page Not Found.</p>
        </section>
      </div>
    )
  }
}

export default NotFound;
