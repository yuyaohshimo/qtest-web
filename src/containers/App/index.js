import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'

import { init } from '../../actions/app';

import Loading from '../../components/Loading';
import ErrorModal from '../../components/ErrorModal';

class App extends Component {
  render() {
    return (
      <div>
        {this.props.children}
        {this.props.error ? <ErrorModal error={this.props.error} init={this.props.init} /> : null}
        {this.props.loading ? <Loading /> : null}
      </div>
    )
  }
}

App.propTypes = {
  error: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.bool
  ])
}

const mapStateToProps = (state, ownProps) => ({
  error: state.global.error,
  loading: state.global.loading
});

function mapDispatchToProps(dispatch) {
  return {
    init: bindActionCreators(init, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
