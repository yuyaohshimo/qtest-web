export const LOADING_START = 'app/LOADING_START';
export const LOADING_END = 'app/LOADING_END';
export const ERROR = 'app/ERROR';
export const INIT = 'app/INIT';
export const START = 'app/START';
export const NEXT = 'app/NEXT';
export const SEND_SCORE = 'app/SEND_SCORE';
export const SEND_SCORE_SUCCESS = 'app/SEND_SCORE_SUCCESS';
