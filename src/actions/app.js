import {
  LOADING_START,
  LOADING_END,
  ERROR,
  START,
  INIT,
  NEXT,
  SEND_SCORE,
  SEND_SCORE_SUCCESS
} from '../constants/app';

export const startLoading = () => ({ type: LOADING_START })
export const endLoading = () => ({ type: LOADING_END })
export const error = (error) => ({ type: ERROR, error })
export const start = () => ({ type: START })
export const next = () => ({ type: NEXT })
export const init = () => ({ type: INIT })
export const sendScore = () => ({ type: SEND_SCORE })
export const scoreSent = () => ({ type: SEND_SCORE_SUCCESS })
