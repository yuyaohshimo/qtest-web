import {
  SAVE
} from '../constants/user.js';

export const save = user => ({ type: SAVE, user })
