import {
  LOAD_RANKING,
  LOAD_RANKING_SUCCESS
} from '../constants/ranking';

export const loadRanking = () => ({ type: LOAD_RANKING })
export const rankingLoaded = ranking => ({ type: LOAD_RANKING_SUCCESS, ranking })
