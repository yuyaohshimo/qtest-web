import {
  LOAD_QUESTIONS_SUCCESS
} from '../constants/question.js';

export const questionsLoaded = questions => ({ type: LOAD_QUESTIONS_SUCCESS, questions })
