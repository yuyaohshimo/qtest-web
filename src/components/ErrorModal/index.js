import React, { Component } from 'react';
import { browserHistory } from 'react-router';

class ErrorModal extends Component {

  constructor(props) {
    super(props);

    this.gotoTop = this.gotoTop.bind(this);
  }

  gotoTop() {
    this.props.init();
    browserHistory.push('/');
  }

  render() {
    return (
      <div className="modal is-active">
        <div className="modal-background"></div>
        <div className="modal-content">
          <div className="box">
            <p className="title">{this.props.error}</p>
            <div className="control">
              <button className="button is-primary" onClick={this.gotoTop}>Top</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ErrorModal.propTypes = {
  error: React.PropTypes.string,
  init: React.PropTypes.func
}

export default ErrorModal;
