import React, { Component } from 'react';

import './styles.css';

class Loading extends Component {
  render() {
    return (
      <div id="loading">
        <span>
          <i className="fa-spin fa fa-circle-o-notch"></i>
        </span>
      </div>
    )
  }
}

export default Loading;
