import React, { Component } from 'react';
import { browserHistory } from 'react-router';

import './styles.css';

class ResultModal extends Component {

  gotoTop() {
    browserHistory.push('/');
  }

  render() {
    let content;
    const {
      messageType,
      answer,
      sendScore,
      scoreSent
    } = this.props;

    if (messageType === 'CORRECT') {
      content = (
        <div className="box">
          <p className="title is-correct">That's correct.</p>
        </div>
      );
    } else if (messageType === 'INCORRECT') {
      content = (
        <div className="box">
          <p className="title is-incorrect">That's incorrect.</p>
          <p className="subtitle">The answer is {answer}.</p>
          <div className="control">
            <div className="block">
              <button className="button is-primary" onClick={this.gotoTop}>Top</button>
              <button className="button is-primary is-outlined" onClick={sendScore} disabled={scoreSent}>Send Score</button>
            </div>
          </div>
        </div>
      )
    }

    return (
      <div className="modal is-active">
        <div className="modal-background"></div>
        <div className="modal-content">
          {content}
        </div>
      </div>
    )
  }
}

ResultModal.propTypes = {
  messageType: React.PropTypes.string,
  sendScore: React.PropTypes.func,
  scoreSent: React.PropTypes.bool,
  answer: React.PropTypes.string,
}

export default ResultModal;
