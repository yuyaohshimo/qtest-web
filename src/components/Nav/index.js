import React, { Component } from 'react';
import { browserHistory } from 'react-router';

import './styles.css';

class Nav extends Component {

  constructor(props) {
    super(props);
    this.onToggleNav = this.onToggleNav.bind(this);
    this.state = { isActiveMenu: false };
  }

  onToggleNav() {
    this.setState({
      isActiveMenu: !this.state.isActiveMenu
    });
  }

  goto(path) {
    browserHistory.push(path);
  }

  render() {
    const toggleClassNames = (this.state.isActiveMenu) ? 'nav-toggle is-active' : 'nav-toggle';
    const navRightClassNames = (this.state.isActiveMenu) ? 'nav-right nav-menu is-active' : 'nav-right nav-menu';
    return (
      <nav className="nav">
        <div className="nav-left">
          <a className="nav-item is-brand" onClick={() => this.goto('/')}>
            <h1 className="title">Q Test</h1>
          </a>
        </div>
        <span className={toggleClassNames} onClick={this.onToggleNav}>
          <span></span>
          <span></span>
          <span></span>
        </span>
        <div className={navRightClassNames}>
          <a className="nav-item" onClick={() => this.goto('/ranking')}>
            <span className="icon">
              <i className="fa fa-trophy"></i>
            </span>
            <span>Ranking</span>
          </a>
        </div>
      </nav>
    )
  }
}

Nav.propTypes = {
}

export default Nav;
