import WtoN from 'words-to-num';

export default (question, answer) => {
  let qAnswer = question.answer;
  let uAnswer = answer;

  // to lowecase
  qAnswer = qAnswer.toLowerCase();
  uAnswer = uAnswer.toLowerCase();

  // remove period
  uAnswer = uAnswer.replace(/\./g, '');

  // remove 'a '
  uAnswer = uAnswer.replace(/a /g, '');

  // word to number
  qAnswer = WtoN.convert(qAnswer) || qAnswer;
  uAnswer = WtoN.convert(uAnswer) || uAnswer;

  return qAnswer === uAnswer;
}
