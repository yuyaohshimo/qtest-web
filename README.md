# qtest-web

## Requirements

- node.js (>=v4.x)
- npm (v3.x)

## Getting Started

Install packages:

```
npm install
```

Run server on `localhost:8080`:

```
npm start
```

Please run qtest-rails on `localhost:3000` at the same time.

## E2E Test on Chrome

Setup webdriver:

```
npm run setup:webdriver
```

Run webdriver:

```
npm run start:webdriver
```

Run mocking server and build app:

```
npm run start:mock
```

Run E2E test using nightwatch:

```
npm run test:e2e
```
